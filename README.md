[![Build Status][travis-badge]][travis-link]
[![Slack Room][slack-badge]][slack-link]

# fish-env

My personal fish-shell environment settings

## Install

With [fisherman]

```
fisher angstbear/fish-env
```

[travis-link]: https://travis-ci.org/angstbear/fish-env
[travis-badge]: https://img.shields.io/travis/angstbear/fish-env.svg
[slack-link]: https://fisherman-wharf.herokuapp.com
[slack-badge]: https://fisherman-wharf.herokuapp.com/badge.svg
[fisherman]: https://github.com/fisherman/fisherman
