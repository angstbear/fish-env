# fish 2.2.0 does not include native snippet support. Upgrade to fish >= 2.3.0
# or append the following code to your ~/.config/fish/config.fish.

# for file in ~/.config/fish/conf.d/*.fish
#     source $file
# end


## ENV
set -U VISUAL emacs
set -U EDITOR $VISUAL

switch (uname)
case Darwin
    set __fish_prompt_hostname "mac-fnord"
    set -x PATH /Applications/Postgres.app/Contents/Versions/9.4/bin $PATH
case Linux
end

## Abbreviations
abbr -a v="vim"
abbr -a em="emacs"
abbr -a e="emacs"
abbr -a ..="cd .."
abbr -a ...="cd ../.."
abbr -a -="cd -"

abbr -a gita="git add"
abbr -a gitb="git branch"
abbr -a gitc="git commit"
abbr -a gitco="git checkout"
abbr -a gitl="git log"
abbr -a gits="git status"
abbr -a ga="git add"
abbr -a gb="git branch"
abbr -a gc="git commit"
abbr -a gco="git checkout"
abbr -a gl="git log"
abbr -a gs="git status"
